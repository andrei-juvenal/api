from pydantic import BaseSettings
from sqlalchemy.ext.declarative import declarative_base


class Settings(BaseSettings):
    """
    Configurações gerais usadas na aplicação
    """
    API_V1_STR: str = '/api/v1'
   
    #credencias geradas pelo render colque a do banco que vocês criaram "postgresql+asyncpg://usuario do banco:senha@postgres.render.com:5432/nome da base de dados"
    DB_URL: str = "postgresql+asyncpg://ra_22204211_user:RrT0zuOT6ZirbQj959lxL8snaBwegT1x@postgres.render.com:5432/ra_22204211"
    DBBaseModel = declarative_base()

    class Config:
        case_sensitive = True


settings = Settings()
